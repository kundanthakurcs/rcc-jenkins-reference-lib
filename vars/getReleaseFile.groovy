#!/usr/bin/env groovy

def call(String repoName,String nexusLinkprefix, String app, String vers){
    fileCount = sh (script: "curl -s --list-only https://nexus.prod.cloud.fedex.com:8443/nexus/service/rest/repository/browse/${repoName}/${nexusLinkprefix}/${app}/${vers}/ | egrep -i '\\..ar</a>' | wc -l",
                    returnStdout: true)
    echo "file count is $fileCount"
    if (fileCount.trim().toInteger() >0){
        // get File
        fileName = sh (script: "curl -s --list-only https://nexus.prod.cloud.fedex.com:8443/nexus/service/rest/repository/browse/${repoName}/${nexusLinkprefix}/${app}/${vers}/ | egrep -i '\\..ar</a>' | awk -v FS='href=\"' '{print \$2}' | awk -v FS='\">' '{print \$1}'",
                    returnStdout: true).trim()
        deployFileClean = fileName.substring(fileName.lastIndexOf(app))
        sh """
             WARFILE=\$(echo $fileName)
             wget -nc -q -O ./$deployFileClean \$WARFILE
             ls -al 
        """
        return "./${deployFileClean}"
     }else{
        return ''
     }
    
}