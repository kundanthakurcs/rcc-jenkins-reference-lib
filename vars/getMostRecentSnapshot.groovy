#!/usr/bin/env groovy

def call(String nexusSnapshotRepoUrl,String nexusReleaseRepoUser, String nexusReleaseRepoPass,String appName, String version, String nexusGroup)
{
     script{
        if(!version.endsWith("-SNAPSHOT")){
            version += "-SNAPSHOT"
        }    
        nexusLink = "$nexusGroup".trim().replaceAll('\\.','/')
        wGeturl="${nexusSnapshotRepoUrl}/${nexusLink}/${appName}/$version/maven-metadata.xml"
        echo "Retrieving the most recent ${appName} version from ${nexusSnapshotRepoUrl} maven-metadata.xml" 
        sh"""
            [[ -f ./maven-metadata.xml ]] && rm ./maven-metadata.xml || echo ' '
            wget -nc --user=${nexusReleaseRepoUser} --password=${nexusReleaseRepoPass} ${wGeturl}
            ls -al 
            echo ${version}
        """
        versionNumber = getVersionNumber("$version")
        releaseFileFolder = sh (script:"cat maven-metadata.xml | egrep -iw '\\s*<value>${versionNumber}-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].[0-9][0-9][0-9][0-9][0-9][0-9]-[0-9]*' | awk -v FS='<value>' '{print \$2}' | awk -v FS='</value>' '{print \$1 }' | sort -r | head -n1",
                returnStdout: true).trim()
        fileType = sh (script:"cat maven-metadata.xml | egrep -iw '\\s<extension>.ar*' | awk -v FS='<extension>' '{print \$2}' | awk -v FS='</extension>' '{print \$1}'",
                returnStdout: true).trim()
        releaseFileName = appName +'-'+releaseFileFolder+'.'+fileType
        wGetUrl="${nexusSnapshotRepoUrl}/${nexusLink}/${appName}/$version/${releaseFileName}"
        echo "url = ${wGetUrl}"
        echo "$releaseFileName"
        sh """
            [[ -f ./$appName.* ]] && rm ./$appName.* || echo ' '
            wget  -np --user=${nexusReleaseRepoUser} --password=${nexusReleaseRepoPass} -O ./$appName.$fileType $wGetUrl
            ls -al ./$appName.$fileType
        """
        stash includes: "**/", name: 'deployFile'  
        return "./$appName.$fileType"
     }
}