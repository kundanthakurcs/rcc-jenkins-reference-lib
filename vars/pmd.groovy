#!/usr/bin/env groovy

def call(pmdParm)
{
    if (pmdParm.length() > 0){
        stash includes: '**/target/pmd.xml', name: 'pmd'
    }
}