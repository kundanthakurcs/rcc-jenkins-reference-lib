#!/usr/bin/env groovy

def call(Object pipeVars, String appName, String deployFileName, String deploy_target, String deploy_release_version, String start_stop_choice, String nexusGroup){
    echo "PCF Deploy & Prep"
        echo deploy_target
        //selecting and deploying to pcf deployment target
		//function
        selectPcfTarget(deploy_target, deploy_release_version, pipeVars, appName, "$nexusGroup", deployFileName)

	    if (deploy_target == 'Stage' || deploy_target == 'Development' || deploy_target == 'Release') {
           	if (pipeVars.generalSettings.smokeTest.trim().length() > 0){
           		def aLevel='dev'
           			if (deploy_target == 'Stage'){
           				aLevel='stage'
           			}
           			if (deploy_target == 'Release'){
           				aLevel='test'
           			}
           				executeSmokeTests(aLevel, pipeVars.generalSettings.smokeTest)
           	}
	    }
        //pcf start/stop based off of build param
		//function
        pcfStartStop(start_stop_choice, pipeVars, appName)
}