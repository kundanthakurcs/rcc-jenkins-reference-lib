#!/usr/bin/env groovy

def call()
{
    def surefireData = ''
    def hasSurefireReport = false
    surefireDirectories = sh (script: "find . -type d  -name surefire-reports -print", returnStdout: true).trim().split()
    for (i = 0; i <surefireDirectories.size(); i++) {
        def currentSurefireDirectories = surefireDirectories[i]
        if (currentSurefireDirectories!=null && currentSurefireDirectories !=''){
            surefireData = sh (script: "ls -l ${currentSurefireDirectories} | grep -i '.xml' | wc -l", returnStdout: true).trim()
            }
        
        if (surefireData !=null && surefireData != '' && surefireData.toInteger() > 0){
            hasSurefireReport = true
        }
        echo "${currentSurefireDirectories} surefireData = ${surefireData} "
    }
                    
    echo "hasSurefireReport = ${hasSurefireReport}"
    if (hasSurefireReport){
        stash includes: '**/target/surefire-reports/*.xml', name: 'surefire'
    }
    return hasSurefireReport
}