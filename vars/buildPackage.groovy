#!/usr/bin/env groovy

def call(String pathToMainPom, String version){
  echo "Build Package"
    unstash 'settings'
    echo "build command: mvn -s ./settings.xml -X -B -U -f ${pathToMainPom} -DskipTests=true -DskipITs=true -fae clean install"
    sh """
        pwd
        ls -al
        cat ./settings.xml
        mvn -s ./settings.xml -X -B -U -f ${pathToMainPom} -DskipTests=true -DskipITs=true -Dlabel=${version} -Dstage.dir=. -fae clean install
    """
    stash includes: "**/", name: 'source'  
}