#!/usr/bin/env groovy

def call(String level, String startOrStop,  String cfApi, String cfOrg, String cfSpace, String pamId, String app, String sourceCodeRepo){
    String stageTitle = "$startOrStop ${level}"
    stage("${stageTitle}"){
        withEnv(["STARTSTOP=${startOrStop}", "CF_API=${cfApi}", 
            "CF_ORG=${cfOrg}", "CF_SPACE=${cfSpace}", "PAMID=${pamId}", "APPID=${app}", "GIT_URL=${sourceCodeRepo}"]){
            echo "call pcfDeploy with url = $CF_API , space = $CF_SPACE pamid = $PAMID"
            pcfDeploy pamId: "$PAMID",
            url: '$CF_API',
            space: '$CF_SPACE',
            cfcmd: 'services',
            extCfCmd: ['version', 'target']
            echo "call cf $STARTSTOP with $CF_ORG"
            sh """
                #!/bin/bash
                PATH=${PATH}:${WORKSPACE}
                cf $STARTSTOP $APPID
            """
        }
    }
}