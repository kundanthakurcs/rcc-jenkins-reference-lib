#!/usr/bin/env groovy

def call(String pathToMainPom)
{
    script{
        pom = readMavenPom file : pathToMainPom
        modules = pom.getModules()
        return modules
    }
}