#!/usr/bin/env groovy

def call(String appName){
    withEnv(["appName=${appName}"]){
        sh '''
        oldFile=$(ls ./${appName}*.*ar 2> /dev/null | wc -l)
        echo "oldFile is $oldFile"
        [[ $oldFile != "0" ]] && rm ./${appName}*.*ar 2> /dev/null  || echo ' no old build file to delete'
        '''

        script {
            list_of_sources = sh(script: 'find . -name src -type d', returnStdout: true)
            sh """
                echo "$list_of_sources"
            """
            list_of_sources = list_of_sources.trim()
            list_of_sources = list_of_sources.split()
            if(list_of_sources.size() >= 1)
            {
                for(i = 0; i < list_of_sources.size(); i++)
                {
                    deleteSource = list_of_sources[i];
                    sh """
                        echo "Deleting all files from $deleteSource"
                        rm -fr -- $deleteSource
                    """
                }
            }
            else
            {
                sh """
                echo "Nothing to delete $list_of_sources"
                """
            }

            list_of_sources = sh(script: 'find . -name target -type d', returnStdout: true)
            sh """
                echo "$list_of_sources"
            """
            list_of_sources = list_of_sources.trim()
            list_of_sources = list_of_sources.split()
            if(list_of_sources.size() >= 1)
            {
                for(i = 0; i < list_of_sources.size(); i++)
                {
                    deleteSource = list_of_sources[i];
                    sh """
                        echo "Deleting all files from $deleteSource"
                        rm -fr -- $deleteSource
                    """
                }
            }
            else
            {
                sh """
                echo "Nothing to delete $list_of_sources"
                """
            }
            sh """
                ls -lahR
            """
        }
    }
}