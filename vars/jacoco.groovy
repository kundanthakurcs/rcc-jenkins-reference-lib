#!/usr/bin/env groovy

def call()
{
    def hasJacoco = false
    def jacocoData = sh (script: "find . -name jacoco.exec -print | wc -l", returnStdout: true).trim()
    echo "jacocoData = ${jacocoData}"
    if (jacocoData !=null && jacocoData != '' && jacocoData.toInteger() > 0){
        hasJacoco = true
    }
     echo "hasJacoco = ${hasJacoco}"
    if (hasJacoco){
        stash includes: '**/target/jacoco.exec', name: 'unitCodeCoverage'
    }
    return hasJacoco
}