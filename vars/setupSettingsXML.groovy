#!/usr/bin/env groovy

def call()
{
    // get and store settings.xml from config files
    configFileProvider([configFile(fileId: 'settings.xml', variable: 'settings_config')]) {
        sh """
        ls -al
        [[ -f ./settings.xml ]] && rm ./settings.xml || echo " ./settings.xml does not exists - copying over"
        """
        sh"""
        cp $settings_config  ./settings.xml
        """
        stash includes: "**/settings.xml", name: "settings"
    }
}