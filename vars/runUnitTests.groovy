#!/usr/bin/env groovy

def call(pathToMainPom, pmdParm, version)
{
    unstash 'settings'
    sh """
        mvn -s ./settings.xml -f ${pathToMainPom} -DskipTests=false org.jacoco:jacoco-maven-plugin:prepare-agent $pmdParm test -Dlabel=$version
        find . -type d -name surefire-reports -print
        find . -name jacoco.exec -print
        ls -al **/**/
    """
}