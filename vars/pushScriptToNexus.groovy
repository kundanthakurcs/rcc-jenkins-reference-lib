#!/usr/bin/env groovy

def call(String appName, String version, String pathToScripts, Object L3, Object L4, Object L5, Object L6, Object PROD, String smokeTestGroovyPath)
{
    script{
        String data = "{" + getProfileJsonString("L3", L3) + getProfileJsonString("L4", L4) + getProfileJsonString("L5", L5) + getProfileJsonString("L6", L6) + getProfileJsonString("Prod", PROD) + "<replace-quote>startscript<replace-quote>: <replace-quote>start.sh<replace-quote>,<replace-quote>stopscript<replace-quote>: <replace-quote>stop.sh<replace-quote>, <replace-quote>smoketest<replace-quote>: <replace-quote>smoketest.groovy<replace-quote>}"
        echo "$data"
        data = data.replaceAll("<replace-quote>", "\"")
        echo "$data"
        sh """
            [[ -f deployment.json ]] && rm deployment.json || echo "Success"
            echo $smokeTestGroovyPath
        """        
        writeFile file: "deployment.json", text: "$data"
        
        
        if(smokeTestGroovyPath != "" && smokeTestGroovyPath != null)
        {
            sh """
                cat deployment.json
                cp $pathToScripts/start.sh start.sh
                cp $pathToScripts/stop.sh stop.sh
                cp $smokeTestGroovyPath smoketest.groovy
                zip ${appName}-${version}.zip deployment.json start.sh stop.sh smoketest.groovy
            """
        }
        else
        {
            sh """
                cat deployment.json
                cp $pathToScripts/start.sh start.sh
                cp $pathToScripts/stop.sh stop.sh
                zip ${appName}-${version}.zip deployment.json start.sh stop.sh 
            """
        }
        stash includes: "${appName}-${version}.zip", name: 'scm-deployment-descriptor'  

    }
}


