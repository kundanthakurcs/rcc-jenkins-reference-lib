#!/usr/bin/env groovy

def call(String pathToMainPom, boolean hasJacoco, boolean hasSurefire, String appName, String version){
    echo "SonarQube"
    withSonarQubeEnv('SonarQube'){
        unstash 'settings'
        unstash 'source'
        script{
            if(hasJacoco){
                unstash 'unitCodeCoverage'
            }
            if(hasSurefire)
            {
                unstash 'surefire'
            }
            sh """
            mvn -s ./settings.xml -f ${pathToMainPom} sonar:sonar \
            -Dsonar.projectName=$appName \
            -Dsonar.projectKey=$appName \
            -Dsonar.projectVersion=$version \
            -Dsonar.jacoco.reportPaths=target/jacoco.exec \
            -Dsonar.junit.reportPaths=target/surefire-reports
            
            """
        }
    }
}