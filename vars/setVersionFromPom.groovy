#!/usr/bin/env groovy

def call(String pathToMainPom)
{
    script{
        sh 'pwd'
        sh 'ls -lah'
        echo "$pathToMainPom versionsss"
        pom = readMavenPom file : pathToMainPom
        version = pom.getVersion()
        if(version.contains("\$")){
            //set default version
            version = "1.0.0"
        }
        echo "$version FIND ME"
        return version
    }
}