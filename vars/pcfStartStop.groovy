#!/usr/bin/env groovy

def call(String start_stop_choice, Object pipeVars, String appName){
    if(start_stop_choice == 'Start' || start_stop_choice == 'Stop'){
            if(params.start_stop_choice == 'Start' ){
				echo "Start Script Running"
                executePcfStartStop('Release', 'start', pipeVars.levels.rel.rel.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.rel.rel.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.applicationSettings.sourceCodeRepo)
            }
            else if(start_stop_choice == 'Stop'){
                echo "Stop Script Running"
                executePcfStartStop('Release', 'stop', pipeVars.levels.rel.rel.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.rel.rel.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.applicationSettings.sourceCodeRepo)
            }
            // executeSmoketest(aLevel, pipeVars.generalSettings.smokeTest)
        }
}