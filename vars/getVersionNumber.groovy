#!/usr/bin/env groovy

def call(String vers){
		tmpNum = vers.trim().reverse().find('TOHSPANS-')
		returnVers = vers.trim().reverse().drop(tmpNum.length())
		return returnVers.reverse()
}