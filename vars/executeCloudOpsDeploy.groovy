#!/usr/bin/env groovy

def call(String serverName, String deployPath, String deployFileName, String appName, String pathToScripts, String sshKey, String version)
{
    echo serverName
    withCredentials([sshUserPrivateKey(credentialsId: sshKey, keyFileVariable: 'CLOUDOPS_SSH_KEY')]) {
        script{
            mainSSHCommand = "ssh -o StrictHostKeyChecking=no -i $CLOUDOPS_SSH_KEY ${serverName} -oUserKnownHostsFile=/dev/null"
            sh """
                ${mainSSHCommand} '[[ -d ${deployPath} ]] && echo "Deploy Path is ${deployPath}" || mkdir ${deployPath}'
            """
            current = sh (script:"${mainSSHCommand} 'readlink -f ${deployPath}current'", returnStdout: true).trim()
            sh """
                ${mainSSHCommand} '[[ -f ${deployPath}current/scripts/stop.sh ]] && ${deployPath}current/scripts/stop.sh || echo "Success"'
                ${mainSSHCommand} '[[ -d ${deployPath}${version}-prev ]] && rm -r ${deployPath}${version}-prev || echo "Success"'
                ${mainSSHCommand} '[[ -d ${deployPath}${version}-prev ]] && rmdir ${deployPath}${version}-prev || echo "Success"'
                ${mainSSHCommand} '[[ -d ${deployPath}${version} ]] && mv ${deployPath}${version} ${deployPath}${version}-prev || echo "Success"'
            """
            if((deployPath + version) == current){
                sh """
                    ${mainSSHCommand} 'ln -sfn ${current}-prev ${deployPath}previous'
                """
            }
            else{
                sh """
                    ${mainSSHCommand} 'ln -sfn ${current} ${deployPath}previous'
                """
            }
            fileType = deployFileName.reverse().take(3).reverse()
            sh """
                ${mainSSHCommand} 'mkdir -p ${deployPath}${version}'
                ${mainSSHCommand} 'mkdir -p ${deployPath}${version}/scripts'
                ${mainSSHCommand} 'ln -sfn ${deployPath}${version} ${deployPath}current'
                scp -o StrictHostKeyChecking=no -i $CLOUDOPS_SSH_KEY $deployFileName ${serverName}:${deployPath}current/${appName}.${fileType}
                scp -o StrictHostKeyChecking=no -i $CLOUDOPS_SSH_KEY $pathToScripts/start.sh ${serverName}:${deployPath}current/scripts/start.sh
                scp -o StrictHostKeyChecking=no -i $CLOUDOPS_SSH_KEY $pathToScripts/stop.sh ${serverName}:${deployPath}current/scripts/stop.sh
                scp -o StrictHostKeyChecking=no -i $CLOUDOPS_SSH_KEY $pathToScripts/status.sh ${serverName}:${deployPath}current/scripts/status.sh
                ${mainSSHCommand} 'chmod 774 ${deployPath}current/scripts/*'
                ${mainSSHCommand} '${deployPath}current/scripts/start.sh'
            """
        }
    }

}