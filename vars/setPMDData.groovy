#!/usr/bin/env groovy

def call(pmdRuleSet)
{
    pmdParm = ""
    if(pmdRuleSet != null && pmdRuleSet.trim().length() > 0){
        pmdParm = "pmd:pmd -Dpmd.ruleset="+pmdRuleSet.trim()
    }
    else{
        pmdParm = "pmd:pmd"
    }
    return pmdParm
                    
}