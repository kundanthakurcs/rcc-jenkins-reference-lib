#!/usr/bin/env groovy

def call(String appName){
    withEnv(["appName=${appName}"]){
        script{
            echo "THE RECEIVED APPNAME IS = $appName"
            sh '''
            echo $appName
            rm -f -- riv_smoketest-0.0.1-2018*.jar 
            rm -f -- noi-smoketest-0.0.1-2018*.jar 
            '''
            deployFileName = sh(script: 'find . -name $appName*.?ar | grep -vi \"sources\" || true', returnStdout: true)
            deployFileName = deployFileName.trim()
            deployFileName = deployFileName.split();
            if(deployFileName.size() > 1)
            {
                min_size_str = deployFileName[0]
                for(i = 0; i < deployFileName.size(); i++)
                {
                    if(deployFileName[i].length() < min_size_str.length())
                    {
                        min_size_str = deployFileName[i]
                    }
                }
                deployFileName = min_size_str
            }
            else if(deployFileName.size() < 1)
            {
                deployFileName = null
            }
            else{
                deployFileName = deployFileName[0]
            }
            echo "===> got deploy-able file ${deployFileName}"
            return deployFileName
        }
    }   
}