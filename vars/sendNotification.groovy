#!/usr/bin/env groovy

def call(String type, String scenario, String emailTo, String emailBody, String app){
    if (scenario.indexOf(type) >= 0){
    
                mail(to: "${emailTo}", subject:"${app} Pipeline ${type}",
                    body: "Job ${emailBody}")
    } 
} 