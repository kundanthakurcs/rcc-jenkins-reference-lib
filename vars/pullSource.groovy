#!/usr/bin/env groovy

def call(appName, sourceCodeSshKey, sourceCodeRepo, selectedBranch){
    echo "Pull Source"
    script{
        if(selectedBranch == null || selectedBranch == ""){
            echo "selected branch not specified, defaulting to master"
            selectedBranch = "master"
        }
        // get code from git
        if(sourceCodeSshKey == null || sourceCodeSshKey == ""){
            //pull code from conexus
            echo "conexus repository: ===> getting ${appName} source code from ${sourceCodeRepo}"              
            git url: sourceCodeRepo, credentialsId: 'jenkinsSCM'
        }
        else{
            // put code from gitlab
            if(selectedBranch == null || selectedBranch == ""){
                selectedBranch = "master";
            }
            echo "gitlab repository ===> getting ${appName} source code from ${sourceCodeRepo}/${selectedBranch}"
            checkout poll: false, scm: [
                $class: 'GitSCM', branches: [[name: selectedBranch] ], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: sourceCodeSshKey, url: sourceCodeRepo]]]              
            // git changelog: false, credentialsId: sourceCodeSshKey, poll:false, url: sourceCodeRepo, branch: selectedBranch
        sh """
        git branch
        ls -al
        """
        }
        
    }
}