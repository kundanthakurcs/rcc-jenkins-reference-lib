#!/usr/bin/env groovy

def call(String appName, String version, String emailNotify, Object L3, Object L4, Object L4_PGH, Object L4_UTE, Object PROD, Object PROD_EDC, String buildServerJava, String buildServerWebLogic)
{
    script{
        String data = "{" + getProfileJsonStringWebLogic("L3", L3) + getProfileJsonStringWebLogic("L4", L4) +getProfileJsonStringWebLogic("L4-PGH", L4_PGH) +getProfileJsonStringWebLogic("L4-UTE", L4_UTE) +getProfileJsonStringWebLogic("PROD", PROD) +getProfileJsonStringWebLogic("PROD-EDC", PROD_EDC) + "<replace-quote>appName<replace-quote>: <replace-quote>"+appName+"<replace-quote>,<replace-quote>emailNotify<replace-quote>: <replace-quote>"+emailNotify+"<replace-quote>, <replace-quote>buildServerJava<replace-quote>: <replace-quote>"+buildServerJava+"<replace-quote>, <replace-quote>buildServerWebLogic<replace-quote>: <replace-quote>"+buildServerWebLogic+"<replace-quote>}";
        echo "$data"
        data = data.replaceAll("<replace-quote>", "\"")
        echo "$data"
        sh """
            [[ -f deployment.json ]] && rm deployment.json || echo "Success"
        """        
        writeFile file: "deployment.json", text: "$data"
        
        sh """
            cat deployment.json
            zip ${appName}-${version}.zip deployment.json
        """
        
        stash includes: "${appName}-${version}.zip", name: 'scm-deployment-descriptor'  

    }
}


