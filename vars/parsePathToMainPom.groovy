#!/usr/bin/env groovy

def call(String pathToMainPom){
    script{
        finalPathToMainPom = "./"
        if(pathToMainPom != null && pathToMainPom != ""){
            if(pathToMainPom.startsWith("./")){
                finalPathToMainPom  = pathToMainPom
            }
            else{
                finalPathToMainPom = finalPathToMainPom + pathToMainPom
            }
        }
        if(finalPathToMainPom.endsWith("/")){
            finalPathToMainPom = finalPathToMainPom[0..-2]   
        }
        if(!finalPathToMainPom.endsWith('pom.xml')){
            finalPathToMainPom = finalPathToMainPom + "/pom.xml"
        }
        echo "===> The path to the main pom file is ${finalPathToMainPom}" 
        pathToMainPom = finalPathToMainPom
        return finalPathToMainPom
    } 
}