#!/usr/bin/env groovy

def call(String profile_name, Object profile)
{
    script{
        String json = ""
        if(profile != null)
        {
            json += "<replace-quote>" + profile_name + "<replace-quote>" + ": {"
            if(profile.java_home != null)
            {
                json += "<replace-quote>java_home<replace-quote>:" + "<replace-quote>" + profile.java_home + "<replace-quote>"
            }
            if(profile.profile_name != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>profile_name<replace-quote>:" + "<replace-quote>" + profile.profile_name + "<replace-quote>"
                
            }
            if(profile.deploy_path  != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>deploy_path<replace-quote>:" + "<replace-quote>" + profile.deploy_path + "<replace-quote>"
                
            }
            if(profile.log_path != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>log_path<replace-quote>:" + "<replace-quote>" + profile.log_path + "<replace-quote>"
            }
            if(profile.targets != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>targets<replace-quote>: ["
                for(int i = 0; i < profile.targets.size(); i++){
                    if(json.reverse().take(1).reverse() != '[')
                    {
                        json += ", "
                    }
                    json += "<replace-quote>" + profile.targets[i] + "<replace-quote>"
                }
                json += "]"
            }
            json += "},"
            return json
        }
        else{
            json += "<replace-quote>" + profile_name + "<replace-quote>" + ": {},"
            return json
        }
    }
}