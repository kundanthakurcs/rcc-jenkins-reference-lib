#!/usr/bin/env groovy

def call(String environmentName, Object environment, String appName, String nexusGroup, String version)
{
    script{
        if(!environment){
            return "";
        }
        targets = environment.targets
        regex = "^("
        for(i=0;i<targets.size();i++){
            if(regex.reverse().take(1).reverse() != "(")
            {
                regex += "|"
            }
            if(targets[i].contains("@"))
            {
                regex += targets[i].split('@')[1]
            }
            else{
                regex += targets[i]
            }
        }
        regex += ")\$"
        profileName = environment.profile_name
        logPath = environment.log_path
        java_home = "/bin"
        if (environment.java_home){
            java_home = environment.java_home
        }
        echo " deploy info ${profileName}"
        if_start = 'if [[ <replace-quote>${SERVERNAME}<replace-quote> =~ ' + regex + " ]]; then"
        if_content = """
        export APP_ID=<replace-quote>${appName}<replace-quote>
        export ARTIFACTID=<replace-quote>${appName}<replace-quote>
        export GROUPID=<replace-quote>${nexusGroup}<replace-quote>
        export JAVA_HOME=<replace-quote>${java_home}<replace-quote>
        export LEVEL=<replace-quote>${environmentName}<replace-quote>
        export LOGDIR=<replace-quote>${logPath}<replace-quote>
        export SPRING_PROFILE=<replace-quote>${profileName}<replace-quote>
        export VERSION=<replace-quote>${version}<replace-quote>\n
        """
        if_echo = '''
        echo "${LEVEL}"
        echo "${SPRING_PROFILE}"\n'''
        if_end = "fi\n"
        final_if = if_start + if_content + if_echo + if_end
        return final_if.replaceAll("<replace-quote>", "\"")

    }
    
}