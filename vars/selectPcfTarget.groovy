#!/usr/bin/env groovy

def call(String deploy_target, String deploy_release_version, Object pipeVars, String appName, String nexusGroup, String deployFileName){
    if (deploy_target == 'Stage') {
            nexusLink = nexusGroup.trim().replaceAll('\\.','/')
            echo nexusLink
            echo appName
            echo deploy_release_version
            echo pipeVars.applicationSettings.nexusReleaseRepoUrl
            def releaseFile4Deploy = getReleaseFile(getRepoName(pipeVars.applicationSettings.nexusReleaseRepoUrl), nexusLink, appName, deploy_release_version)
            echo "ready to deploy to stage using $releaseFile4Deploy"
                if (releaseFile4Deploy == ''){
                  	error("Fail to deploy to Staging because $appName release version ${deploy_release_version} not found")
                }else{ 
					executePcfDeployment('Stage',pipeVars.levels.stg.stg.jms, pipeVars.generalSettings.configCredentials, pipeVars.generalSettings.configEnvUsername, pipeVars.generalSettings.configEnvPassword, pipeVars.levels.stg.stg.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.stg.stg.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.levels.stg.stg.manifest, releaseFile4Deploy, pipeVars.applicationSettings.sourceCodeRepo)
                }
        }else{
            if ( deploy_target == 'Production' ){
                nexusLink = nexusGroup.trim().replaceAll('\\.','/')
                def releaseFile4Deploy = getReleaseFile(getRepoName(pipeVars.applicationSettings.nexusReleaseRepoUrl), nexusLink, appName, deploy_release_version)
                echo "ready to deploy to production using $releaseFile4Deploy"
                  	if (releaseFile4Deploy == ''){
                  			error("Fail to deploy to Production because $appName release version ${deploy_release_version} not found")
                  	}else{ 
 						executePcfDeployment('Prod', pipeVars.levels.prod.prod.jms, pipeVars.generalSettings.configCredentials, pipeVars.generalSettings.configEnvUsername, pipeVars.generalSettings.configEnvPassword, pipeVars.levels.prod.prod.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.prod.prod.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.levels.prod.prod.manifest, releaseFile4Deploy, pipeVars.applicationSettings.sourceCodeRepo)
                  	}
            }else{
                if (deploy_target == 'Release' ){
	                nexusLink = nexusGroup.trim().replaceAll('\\.','/')
	                def releaseFile4Deploy = getReleaseFile(getRepoName(pipeVars.applicationSettings.nexusReleaseRepoUrl), nexusLink, appName, deploy_release_version)
	                echo "ready to deploy to Release using $releaseFile4Deploy"
	                  	if (releaseFile4Deploy == ''){
	                  		error("Fail to deploy to Release because $appName release version ${deploy_release_version} not found")
	                  	}else{ 
	 						executePcfDeployment('Rel', pipeVars.levels.rel.rel.jms, pipeVars.generalSettings.configCredentials, pipeVars.generalSettings.configEnvUsername, pipeVars.generalSettings.configEnvPassword, pipeVars.levels.rel.rel.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.rel.rel.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.levels.rel.rel.manifest, releaseFile4Deploy, pipeVars.applicationSettings.sourceCodeRepo)
	                  	}
                }else{ // deploy_target == 'Development'
	                //unstash 'source'
	                	if (pipeVars.generalSettings.mode == 'CICD'){
							echo "debug line 488"
							echo pipeVars.levels.dev.dev.jms
                            executePcfDeployment('dev', pipeVars.levels.dev.dev.jms, pipeVars.generalSettings.configCredentials, pipeVars.generalSettings.configEnvUsername, pipeVars.generalSettings.configEnvPassword, pipeVars.levels.dev.dev.cfApi, pipeVars.generalSettings.cfOrgEai, pipeVars.levels.dev.dev.cfSapce, pipeVars.generalSettings.pamId, appName, pipeVars.levels.dev.dev.manifest, deployFileName, pipeVars.applicationSettings.sourceCodeRepo)	                		
	                	}
	            }
	        }
	    }
}