#!/usr/bin/env groovy

def call(String pathToMainPom)
{
    script{
        pom = readMavenPom file : pathToMainPom
        nexusGroup = pom.getGroupId()
        if(nexusGroup == null)
        {
            nexusGroup = pom.getParent().getGroupId()
        }
        echo "$nexusGroup FIND ME"
        return nexusGroup
    }
}