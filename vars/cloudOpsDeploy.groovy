#!/usr/bin/env groovy

def call(Object pipeVars, String appName, String deployFileName, String version, String nexusGroup){
    echo "CloudOps Deploy & Prep:"
    levels = pipeVars.generalSettings.deployLevels.split('/')
    levels.eachWithIndex { aLevel, i ->
    if ( aLevel == 'L1' || aLevel == 'L2' || aLevel == 'L0'){
        if (pipeVars.levels."${aLevel}"){
            if (pipeVars.generalSettings.mode == 'CICD'){
                activeProfileList = pipeVars.levels."${aLevel}".activeProfiles
                activeProfiles = activeProfileList.split("/")
                echo "activeProfiles $activeProfiles"
                activeProfiles.eachWithIndex{ aProfile, j ->
                    String stageTitle = "Deploy ${aProfile}"
                    stage("${stageTitle}"){
                        echo "Deploying to ${aProfile}"
                        deployPath = pipeVars.levels."${aLevel}"."${aProfile}".deploy_path
                        targets = pipeVars.levels."${aLevel}"."${aProfile}".targets
                        for(i=0;i<targets.size();i++){  
                            String sshKey = pipeVars.generalSettings.sourceCodeSshKey
                            if(pipeVars.generalSettings.deploySshKey != null && pipeVars.generalSettings.deploySshKey != ""){
                                sshKey = pipeVars.generalSettings.deploySshKey
                            }
                            echo "SEARCH FOR: $sshKey"
                            executeCloudOpsDeploy(targets[i], deployPath, deployFileName, appName, pipeVars.applicationSettings.pathToScripts, sshKey, version)
                        }
                    }
                }
            }
            if (pipeVars.generalSettings.smokeTest.trim().length() > 0){
                    executeSmokeTests(aLevel, pipeVars.generalSettings.smokeTest)
            }
        }
        else{
            error("Stopped because level ${aLevel} deployment info is not present")
        }
    }
    else{
        if (pipeVars.generalSettings.mode == 'smokeTestOnly' && pipeVars.generalSettings.smokeTest.trim().length() > 0){
            if ( aLevel == 'L3' || aLevel == 'L4' || aLevel == 'L5' || aLevel == 'L6'){
                    executeSmokeTests(aLevel, pipeVars.generalSettings.smokeTest)
            }
        }
        else{
            echo "===> skipping unauthorized dev deployment of level ${aLevel}"
            }
        }
    }
}