#!/usr/bin/env groovy

def call(String repoUrl){
    lastChar = repoUrl[-1]
    echo "last char is $lastChar"
    tmp = repoUrl
    if (lastChar == '/'){
        tmp = repoUrl[0..-2]
    }
    echo "repoUrl is $tmp"
    return tmp.substring(tmp.lastIndexOf("/")+1) 
}