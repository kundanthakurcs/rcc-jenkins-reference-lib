#!/usr/bin/env groovy

def call(String level, String jmsCredentials, String configCredential, String configEnvUser, String configEnvPass, String cfApi, String cfOrg, String cfSpace, String pamId, String app, String appManifest, String deployFile, String sourceCodeRepo){
    String stageTitle = "Deploy ${level}"

    stage("${stageTitle}"){
        def username=''
        def password=''
        if (configCredential != null && configCredential.trim().length() >0){
            withCredentials([usernamePassword(credentialsId: "$configCredential", usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD' )] ) {
                    username = USERNAME
                    password = PASSWORD

                    echo "Get credentials $configCredential - user = $USERNAME signed to $configEnvUser"
            }
        }
        def jmsusername=''
        def jmspassword=''
        if(jmsCredentials != null && jmsCredentials.trim().length() > 0){
            withCredentials([usernamePassword(credentialsId: jmsCredentials, usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD')]){
                    jmsusername = USERNAME
                    jmspassword = PASSWORD

                    echo "Get credentials test-user-jms - user = $USERNAME "
            }
        }
        withEnv(["CUSER=${username}", "CPASS=${password}", "CF_API=${cfApi}", "CONFIGENVUSER=${configEnvUser}", "CONFIGENVPASS=${configEnvPass}",
            "CF_ORG=${cfOrg}", "CF_SPACE=${cfSpace}", "PAMID=${pamId}",  "APPID=${app}", "MANIFESTFILE=${appManifest}", "DEPLOYFILE=${deployFile}","GIT_URL=${sourceCodeRepo}" ]){
            echo "debug line 802"
            echo "call pcfDeploy with url= $CF_API , space = $CF_SPACE , pamid = $PAMID , sourcecoderepo = $sourceCodeRepo"
            pcfDeploy pamId: "$PAMID",
                    url: '$CF_API',
                    space: '$CF_SPACE',
                    cfcmd: 'services',
                    extCfCmd: ['version', 'target']
            echo "debug line 809"
            if (username != null && username.trim().length() >0){
                echo "call cf push with $CF_ORG with no start first, but set cuser = $CUSER, configUser = $CONFIGENVUSER"
                sh """
                    #!/bin/bash
                    PATH=${PATH}:${WORKSPACE}
                    
                    cf push $APPID -f "${MANIFESTFILE}" -p "${DEPLOYFILE}" --no-start
                    cf set-env ${APPID} "$CONFIGENVUSER" "$CUSER"
                    cf set-env ${APPID} "$CONFIGENVPASS" "$CPASS"
                    cf set-env ${APPID} "JMSUSERNAME" "$jmsusername"
                    cf set-env ${APPID} "JMSPASSWORD" "$jmspassword"
                    cf start ${APPID}
                """
            }else{
                echo "call cf push with $CF_ORG with start"
                sh """
                    #!/bin/bash
                    PATH=${PATH}:${WORKSPACE}
                    cf set-env ${APPID} "JMSUSERNAME" "$jmsusername"
                    cf set-env ${APPID} "JMSPASSWORD" "$jmspassword"\t
                    cf push $APPID -f "${MANIFESTFILE}" -p "${DEPLOYFILE}" 
                """

            }

        }
    }
}