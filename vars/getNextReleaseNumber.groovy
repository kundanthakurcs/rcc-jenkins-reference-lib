#!/usr/bin/env groovy

def call(String repoName, String groupPath, String app, String vers) {
		releaseName = sh (script: "curl -s --list-only  https://nexus.prod.cloud.fedex.com:8443/nexus/service/rest/repository/browse/${repoName}/${groupPath}/${app}/ | egrep -i '>[0-9]+.*.[0-9]+<' | awk -v FS='/\">' '{print \$2}' | awk -v FS='</' '{print \$1}' | sort -V | tail -n1  ",
						returnStdout: true)
	    echo " got current release number $releaseName"
		if (releaseName == null || releaseName.trim().length() <=0){
			return vers;
		}else{
			releaseName = releaseName.trim()
		    lastNum = releaseName.substring(releaseName.lastIndexOf('.')+1)
		    returnStr = releaseName.reverse().drop(lastNum.length())
		    return returnStr.reverse() + (lastNum.toInteger()+1) 
		}
}

// def call(String nexusReleaseRepoUrl, String nexusLink, String appName, String nexusReleaseRepoUser, String nexusReleaseRepoPass, String version) {
// 	    wGeturl="${nexusReleaseRepoUrl}/${nexusLink}/${appName}/$version/maven-metadata.xml"
//         echo "Retrieving the most recent ${appName} version from ${nexusReleaseRepoUrl} maven-metadata.xml" 
//         sh"""
//             [[ -f ./maven-metadata.xml ]] && rm ./maven-metadata.xml || echo ' '
//             wget -nc --user=${nexusReleaseRepoUser} --password=${nexusReleaseRepoPass} ${wGeturl}
//             ls -al 
//             echo ${version}
//         """
//         versionNumber = getVersionNumber("$version")
//         releaseName = sh (script:"cat maven-metadata.xml | egrep -iw '\\s*<value>${versionNumber}-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].[0-9][0-9][0-9][0-9][0-9][0-9]-[0-9]*' | awk -v FS='<value>' '{print \$2}' | awk -v FS='</value>' '{print \$1 }' | sort -r | head -n1",
//                 returnStdout: true).trim()
//         echo " got current release number $releaseName"
// 		if (releaseName == null || releaseName.trim().length() <=0){
// 			return version;
// 		}else{
// 			releaseName = releaseName.trim()
// 		    lastNum = releaseName.substring(releaseName.lastIndexOf('.')+1)
// 		    returnStr = releaseName.reverse().drop(lastNum.length())
// 		    return returnStr.reverse() + (lastNum.toInteger()+1) 
// 		}
// }

def call(String groupPath, String app, String vers) {
    // releaseName = sh (script: "curl -s --list-only  https://nexus.prod.cloud.fedex.com:8443/nexus/content/repositories/fxg-releases/${groupPath}/${app}/ | egrep -i \"/$app/[0-9]+\" | awk -v FS='/</a></td>' '{print \$1 }' | awk -v FS='/\">' '{print \$2}' | sort -r | head -n1",
    releaseName = sh (script: "curl -s --list-only  https://nexus.prod.cloud.fedex.com:8443/nexus/service/rest/repository/browse/fxg-releases/${groupPath}/${app}/ | egrep -i '>[0-9]+.*.[0-9]+<' | awk -v FS='/\">' '{print \$2}' | awk -v FS='</' '{print \$1}' | sort -V | tail -n1 ",
                    returnStdout: true)
    echo " got current release number $releaseName"
    if (releaseName == null || releaseName.trim().length() <=0){
        return vers;
    }else{
        releaseName = releaseName.trim()
        lastNum = releaseName.substring(releaseName.lastIndexOf('.')+1)
        // echo " get lastNum = $lastNum"
        returnStr = releaseName.reverse().drop(lastNum.length())
        return returnStr.reverse() + (lastNum.toInteger()+1) 
    }
}