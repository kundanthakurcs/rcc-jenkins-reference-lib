#!/usr/bin/env groovy

def call(String javaWithPath, String weblogicWithPath, String app, String level, String adminUrl, String userId, String passwd, String cluster, String deployFile)
{
    echo "in function ${deployFile} theLevel ${level} theAdminUrl ${adminUrl}"
    String stageTitle = "Deploy ${level}"
    stage("${stageTitle}"){
        node ('FXG-SCM-DEV'){
            if(!deployFile.contains('wget'))
            {
                unstash 'source'
            }
            unstash 'deployFile'
            unstash 'settings'
            sh """
                env
                . ~/.bash_profile
                ls -al $deployFile           
                echo \"===> Deploy $deployFile to $adminUrl \"             
                $javaWithPath -Xms512M -Xmx512M -cp $weblogicWithPath weblogic.Deployer \
                -debug -remote -verbose -noexit -name $app \
                -targets $cluster -adminurl t3://$adminUrl -username $userId -password $passwd -undeploy
                $javaWithPath -Xms512M -Xmx512M -cp $weblogicWithPath weblogic.Deployer \
                -debug -stage -remote -verbose -upload -name $app -source $deployFile  \
                -targets $cluster -adminurl t3://$adminUrl -username $userId -password $passwd -deploy
            """ 
        }
    }
}


def call(String javaWithPath, String weblogicWithPath, String weblogicAdminWithPort, String releaseFile4Deploy, String appName, String wblCluster, String CONFIG, String KEY)
{
    sh """
        env
        . ~/.bash_profile
        echo \"===> Deploy $releaseFile4Deploy to $weblogicAdminWithPort \"             
        $javaWithPath -Xms512M -Xmx512M -cp $weblogicWithPath weblogic.Deployer \
            -debug -remote -verbose -noexit -name $appName \
            -targets $wblCluster -adminurl t3://$weblogicAdminWithPort -userconfigfile $CONFIG -userkeyfile $KEY -undeploy
        $javaWithPath -Xms512M -Xmx512M -cp $weblogicWithPath weblogic.Deployer \
            -debug -stage -remote -verbose -upload -name $appName -source $releaseFile4Deploy  \
            -targets $wblCluster -adminurl t3://$weblogicAdminWithPort -userconfigfile $CONFIG -userkeyfile $KEY -deploy
    """ 
}


