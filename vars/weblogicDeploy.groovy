#!/usr/bin/env groovy

def call(Object pipeVars, String appName, String deployFileName){
    echo "WebLogic Deploy & Prep"
    levels = pipeVars.generalSettings.deployLevels.split('/')
    echo "levels ${levels}"
    levels.eachWithIndex { aLevel, i ->
        echo " index ${i}"
        if ( aLevel == 'L1' || aLevel == 'L2' || aLevel == 'L0'){
            if (pipeVars.levels."${aLevel}"){
                if (pipeVars.generalSettings.mode == 'CICD'){
                    activeProfileList = pipeVars.levels."${aLevel}".activeProfiles
                    activeProfiles = activeProfileList.split("/")
                    echo "activeProfiles $activeProfiles"
                    activeProfiles.eachWithIndex{ aProfile, j ->
                    
                        adminServerInfo = pipeVars.levels."${aLevel}"."${aProfile}"
                        echo " deploy info ${adminServerInfo}"
                        executeWebLogicDeploy(pipeVars.applicationSettings.buildServerJava, pipeVars.applicationSettings.buildServerWeblogic, appName, aProfile, adminServerInfo.weblogic_admin, adminServerInfo.weblogic_user, adminServerInfo.weblogic_password, adminServerInfo.weblogic_cluster, deployFileName) 
                    }
                }
                if (pipeVars.generalSettings.smokeTest.trim().length() > 0){
                    executeSmokeTests(aLevel, pipeVars.generalSettings.smokeTest)
                }
            }else{
                error("Stopped because level ${aLevel} deployment info is not present")
            }
        }else{
            if (pipeVars.generalSettings.mode == 'smokeTestOnly' && pipeVars.generalSettings.smokeTest.trim().length() > 0){
                if ( aLevel == 'L3' || aLevel == 'L4' || aLevel == 'L5' || aLevel == 'L6'){
                    executeSmokeTests(aLevel, pipeVars.generalSettings.smokeTest)
                }
            }else{
                echo "===> skipping unauthorized dev deployment of level ${aLevel}"
            }
        }
    }
}

def call(String appName, String releaseFile4Deploy, String level, String profileStr, Object pipeVars)
{
    echo " Prepare deployment for ${level}"

    // give SCM control for Prod install
    if ( "${level}" == "PRD" ){
//            notification( 24, "Deploy to production ?")
        notification()
    }

    def profiles = profileStr.split('/')
    profiles.eachWithIndex { aProfile, i ->
        echo " index ${i}, profile ${aProfile}"
        if (pipeVars.levels."${level}"."${aProfile}"){
            echo "Deploying to ${aProfile}"
            String stageTitle = "Deploy ${aProfile}"
            stage("${stageTitle}"){		                	
            
            wblUserConfigFileCredential = pipeVars.levels."${level}"."${aProfile}".weblogic_userConfigFile
            wblUserKeyFileCredential = pipeVars.levels."${level}"."${aProfile}".weblogic_userKeyFile
            wblCluster = pipeVars.levels."${level}"."${aProfile}".weblogic_cluster
            weblogicAdminWithPort = pipeVars.levels."${level}"."${aProfile}".weblogic_admin
            javaWithPath = pipeVars.applicationSettings.buildServerJava
            weblogicWithPath = pipeVars.applicationSettings.buildServerWeblogic
            withCredentials([file(credentialsId: "$wblUserKeyFileCredential", variable: 'KEY'), file(credentialsId: "$wblUserConfigFileCredential", variable: 'CONFIG')]) {

                // WLSdeployer (adminConsoleUrl: "t3://$weblogicAdminWithPort", 
                //	applicationName: "$appName", 
                // 	fileToDeployName: "$releaseFile4Deploy", 
                // 	cluster: "$wblCluster", 
                // 	userKeyFile: KEY, 
                // 	userConfigFile: CONFIG)
                
                
                executeWebLogicDeploy(javaWithPath, weblogicWithPath, weblogicAdminWithPort, releaseFile4Deploy, appName, wblCluster, CONFIG,KEY)
                    
                }
            }
            }else{
            error("Fail to deploy because weblogic profile ${aProfile} information not found in pipeline.json file")
            }
        
    }
}