#!/usr/bin/env groovy

def call(String nexusSnapshotRepoUrl, String nexusReleaseRepoUrl, String  nexusReleaseRepoID,String  nexusReleaseRepoUser,String  nexusReleaseRepoPass,String  appName,String  version,String  nexusGroup, boolean stashed, String userSpecifiedReleaseVersion){
    echo "Nexus - Release"
    script{
        if(nexusSnapshotRepoUrl.endsWith("/")){
            nexusSnapshotRepoUrl = nexusSnapshotRepoUrl[0..-2]  ; 
        }
        tempVersion = version
        if(!version.endsWith("-SNAPSHOT")){
            tempVersion = version + "-SNAPSHOT";
            echo "$tempVersion"
        }
        echo "$tempVersion"
        nexusLink = "$nexusGroup".trim().replaceAll('\\.','/')
        wGeturl="${nexusSnapshotRepoUrl}/${nexusLink}/${appName}/$tempVersion/maven-metadata.xml"
        echo "Retrieving the most recent ${appName} version from ${nexusSnapshotRepoUrl} maven-metadata.xml" 
        try {
        sh"""
            [[ -f ./maven-metadata.xml ]] && rm ./maven-metadata.xml || echo ' '
            wget -nc --user=${nexusReleaseRepoUser} --password=${nexusReleaseRepoPass} ${wGeturl}
            ls -al 
            echo ${version}
        """}
        catch(Exception e){
            echo "Not found in maven or something went wrong"
            return
        }
        versionNumber = getVersionNumber("$tempVersion")
        releaseFileFolder = sh (script:"cat maven-metadata.xml | egrep -iw '\\s*<value>${versionNumber}-[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9].[0-9][0-9][0-9][0-9][0-9][0-9]-[0-9]*' | awk -v FS='<value>' '{print \$2}' | awk -v FS='</value>' '{print \$1 }' | sort -r | head -n1",
                returnStdout: true).trim()
        fileType = sh (script:"cat maven-metadata.xml | egrep -iw '\\s<extension>.ar*' | awk -v FS='<extension>' '{print \$2}' | awk -v FS='</extension>' '{print \$1}'",
                returnStdout: true).trim()

        releaseFileName = appName +'-'+releaseFileFolder+'.'+fileType
        def nextReleaseVersion = getNextReleaseNumber(getRepoName(nexusReleaseRepoUrl), nexusLink, appName, versionNumber)
        if(userSpecifiedReleaseVersion != null && userSpecifiedReleaseVersion != ""){
            nextReleaseVersion = userSpecifiedReleaseVersion;
        }
        def userInput
        timeout(time: 10, unit: 'MINUTES') {
            userInput = input(id: "Release Control", message: "RELEASE ${releaseFileName} from ${nexusSnapshotRepoUrl} repo to new version ${nextReleaseVersion} in ${nexusReleaseRepoUrl}?", ok: 'Submit', 
            parameters: [booleanParam(defaultValue: false, description: 'Please check the Yes box then Submit',name: 'Yes')])
        }
        
        echo "answer is ${userInput}"
        if (userInput == true){
            wGetUrl="${nexusSnapshotRepoUrl}/${nexusLink}/${appName}/$tempVersion/${releaseFileName}"
            echo "url = ${wGetUrl}"
            sh """
				[[ -f ./releasePackage.* ]] && rm ./releasePackage.* || echo ' '
                wget  -np --user=${nexusReleaseRepoUser} --password=${nexusReleaseRepoPass} -O ./releasePackage.$fileType $wGetUrl
                ls -al ./releasePackage.$fileType
            """
            
            relRepoUrl="${nexusReleaseRepoUrl}"
            relRepo=getRepoName(nexusReleaseRepoUrl)
    
            if(nexusReleaseRepoID != null && nexusReleaseRepoID != ""){
                repo = nexusReleaseRepoID
            }
            else{
                repo=getRepoName(nexusReleaseRepoUrl)
            }
            releaseFileName = "./releasePackage.$fileType"
            echo " ===> Ready to Upload $releaseFileName as version ${nextReleaseVersion} to Repo ${relRepoUrl}"              
            unstash 'settings'
            sh """
                mvn deploy:deploy-file -DartifactId=${appName} -Dfile=${releaseFileName} -DgeneratePom=true -DgroupId=$nexusGroup -Dpackaging=${fileType} -DrepositoryId="${repo}" -Durl="${relRepoUrl}" -Dversion="${nextReleaseVersion}" -s ./settings.xml     
                [[ -f $releaseFileName ]] && rm $releaseFileName || echo ' '
                [[ -f ./maven-metadata.xml ]] && rm ./maven-metadata.xml || echo ' '
            """
            if(stashed == true){
                unstash 'scm-deployment-descriptor'
                if(version.trim() != nextReleaseVersion.trim())
                {
                    sh """
                    ls -lah
                        mv ${appName}-${version}.zip ${appName}-${nextReleaseVersion}.zip
                    """
                }
                releaseFileName = "${appName}-${nextReleaseVersion}.zip"
                fileType = "zip"
                sh """
                mvn deploy:deploy-file -DartifactId=${appName}-descriptor -Dfile=${releaseFileName} -DgeneratePom=false -DgroupId=$nexusGroup -Dpackaging=${fileType} -DrepositoryId="${repo}" -Durl="${relRepoUrl}" -Dversion="${nextReleaseVersion}" -s ./settings.xml     
                [[ -f $releaseFileName ]] && rm $releaseFileName || echo ' '
            """
            }
        }else{
            echo " ==> No Nexus RELEASE for ${appName} to version ${nextReleaseVersion}. Canceled by user"
			sh """
				[[ -f ./releasePackage.* ]] && rm ./releasePackage.* || echo ' '
				[[ -f ./maven-metadata.xml ]] && rm ./maven-metadata.xml || echo ' '
			"""
        }

    }
}