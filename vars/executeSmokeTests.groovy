#!/usr/bin/env groovy

def call(String someLevel, String smoketestGrovvyFile){
    echo "Smoke Test for ${someLevel}"
    String stageTitle = "Smoke Test ${someLevel}"
    stage("${stageTitle}"){
        unstash 'source'
        withEnv(["level=${someLevel}"]){
            evaluate readFile(smoketestGrovvyFile)
        }
    }
}

def call(String someLevel, String someVersion, String smoketestGrovvyFile){
    unstash 'source'
                	
	withEnv(["level=${someLevel}", "version=${someVersion}"]){
		echo "$smoketestGrovvyFile"
			evaluate readFile(smoketestGrovvyFile)
	}
}