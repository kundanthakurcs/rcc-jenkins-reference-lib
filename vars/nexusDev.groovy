#!/usr/bin/env groovy

def call(String nexusSnapshotRepoUrl,String nexusSnapshotRepoID,String appName,String deployFileName, String version, String nexusGroup){
    echo "Nexus - Dev"   
    if(deployFileName == null){
        echo "deployFileName is null ${deployFileName}"
        return
    }
    
    sh"""
        ls -al ${deployFileName}
        echo ${WORKSPACE}           
    """
    script{
        fileType = deployFileName.reverse().take(3).reverse()
        echo "upload type ${fileType}"
        repoUrl="${nexusSnapshotRepoUrl}"
        if(nexusSnapshotRepoID != null && nexusSnapshotRepoID != ""){
            repo = nexusSnapshotRepoID
        }
        else{
            repo=getRepoName(nexusSnapshotRepoUrl)
        }
        snapshot_version = version.trim()
        if(!snapshot_version.endsWith("-SNAPSHOT")){
            snapshot_version = snapshot_version + "-SNAPSHOT"
        }
    }
    echo "repo is $repo"
    echo "===> Upload Snapshot from ${deployFileName} to ${repoUrl}"
    unstash 'settings'
    sh"""
        mvn deploy:deploy-file -DartifactId=${appName} -Dfile=${deployFileName} -DgeneratePom=false -DgroupId=${nexusGroup} -Dpackaging=${fileType} -DrepositoryId="${repo}" -Durl="${repoUrl}" -Dversion="${snapshot_version}" -s ./settings.xml    
    """
}