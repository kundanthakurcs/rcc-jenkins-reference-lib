#!/usr/bin/env groovy

def call(Object pipeVars, String appName, String nexusGroup, String version, String pathToScripts)
{
    script {
        output = '''
        #!/bin/bash
        SERVERNAME=$(hostname)
        '''
        if (pipeVars.levels.L0){
            output += createBashStart("L0", pipeVars.levels.L0.L0, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L1){
            output += createBashStart("L1", pipeVars.levels.L1.L1, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L2){
            output += createBashStart("L2", pipeVars.levels.L2.L2, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L3){
            output += createBashStart("L3", pipeVars.levels.L3.L3, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L4){
            output += createBashStart("L4", pipeVars.levels.L4.L4, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L5){
            output += createBashStart("L4", pipeVars.levels.L5.L5, appName, nexusGroup, version)
        }
        if (pipeVars.levels.L6){
            output += createBashStart("L4", pipeVars.levels.L6.L6, appName, nexusGroup, version)
        }
        if (pipeVars.levels.PROD){
            output += createBashStart("PROD", pipeVars.levels.PROD.PROD, appName, nexusGroup, version)
        }
        echo "Part added to script is $output"
        writeFile file: "$pathToScripts/final_start.sh", text: "$output"
        sh """
            cat $pathToScripts/start.sh >> $pathToScripts/final_start.sh
            rm $pathToScripts/start.sh
            mv $pathToScripts/final_start.sh $pathToScripts/start.sh
            cat $pathToScripts/start.sh
        """
    }
}