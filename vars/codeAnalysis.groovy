#!/usr/bin/env groovy

def call(Object pipeVars, boolean hasJacoco, boolean hasSurefireReport){
    echo "Code Analysis"
    script{
        parallel(
            jacoco:{
                if(hasJacoco)
                {
                    unstash 'source'
                    unstash 'unitCodeCoverage'
                    step([$class: 'JacocoPublisher', changeBuildStatus: true, maximumBranchCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxBranchCoverage}", maximumClassCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxClassCoverage}",
                            maximumComplexityCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxComplexityCoverage}", maximumInstructionCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxInstructionCoverage}", 
                            maximumLineCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxLineCoverage}", maximumMethodCoverage: "${pipeVars.codeCoverageSettingsjacocoMaxMethodCoverage}"])
                }
            },
            junit:{
                if(hasSurefireReport){
                    unstash 'surefire'
                    junit '**/target/surefire-reports/*.xml'
                }
            },
            pmd:{
                if(pipeVars.codeCoverageSettingspmdRuleSet != null && pipeVars.codeCoverageSettingspmdRuleSet.trim().length() > 0){
                    unstash 'pmd'
                    step([$class: 'PmdPublisher', canComputeNew: false, defaultEncoding: '', healthy: "${pipeVars.codeCoverageSettingspmdHeathyThreshold}", pattern: '**/pmd.xml',
                            shouldDetectModules: true, thresholdLimit: 'high', unHealthy: "${pipeVars.codeCoverageSettingspmdUnHeathyThreshold}"])
                }
            }
        )
    }
}