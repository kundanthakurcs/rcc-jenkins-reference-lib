#!/usr/bin/env groovy

def call(String profile_name, Object profile)
{
    script{
        String json = ""
        if(profile != null)
        {
            json += "<replace-quote>" + profile_name + "<replace-quote>" + ": {"
            if(profile.weblogic_admin != null)
            {
                json += "<replace-quote>weblogic_admin<replace-quote>:" + "<replace-quote>" + profile.weblogic_admin + "<replace-quote>"
            }
            if(profile.weblogic_userConfigFile != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>weblogic_userConfigFile<replace-quote>:" + "<replace-quote>" + profile.weblogic_userConfigFile + "<replace-quote>"
                
            }
            if(profile.weblogic_userKeyFile  != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>weblogic_userKeyFile<replace-quote>:" + "<replace-quote>" + profile.weblogic_userKeyFile + "<replace-quote>"
                
            }
            if(profile.weblogic_cluster != null)
            {
                if(json.reverse().take(1).reverse() != '{')
                {
                    json += ", "
                }
                json += "<replace-quote>weblogic_cluster<replace-quote>:" + "<replace-quote>" + profile.weblogic_cluster + "<replace-quote>"
            }
            json += "},"
            return json
        }
        else{
            json += "<replace-quote>" + profile_name + "<replace-quote>" + ": {},"
            return json
        }
    }
}